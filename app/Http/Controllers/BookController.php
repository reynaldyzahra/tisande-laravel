<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class BookController extends Controller
{
    //
    public function index()
    {
        $books = Book::all();
        return view('book.index',compact('books'));
    }

    public function create()
    {
        // $categories = Category::all();

        return view('book.create');
    } 
    public function store()
    {
        Book::create([
            'book_cover' => request('book_cover'),
            'book_name' => request('book_name'),
            'slug' => str_slug(request('book_name')),
            'description' => request('description')
        ]);

        return redirect()->route('book.index')->with('success','Buku telah ditambahkan');

    }
    
    public function destroy(Book $book)
    {
       $book->delete();
       return redirect()->route('book.index')->with('danger','data buku yang diminta telah dihapus'); 

       
    }
    
}
