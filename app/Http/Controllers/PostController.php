<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        // dd($posts);
        return view('post.index' ,compact('posts'));
    }


    public function create()
    {
        $categories = Category::all();

        return view('post.create',compact('categories'));
    } 
    public function store()
    {
            Post::create([
                'title' => request('title'),
                'slug' => str_slug(request('title')),
                'content' => request('content'),
                'category_id' => request('category_id')  
            ]);

            return redirect()->route('post.index')->with('success','Post telah ditambahkan');
            
    }
    public function edit(Post $post)
    { 
        $categories = Category::all();

        return view('post.edit',compact('post','categories'))->with('success','data telah berhasil dirubah');
    }
    public function update(Post $post)
    {
     
        $post->update([
            'title' => request('title'),
            'category_id' => request('category_id'),
            'content' => request('content')
        ]);

        return redirect()->route('post.index')->with('success','data telah dirubah');
    }
    public function destroy(Post $post)
    {
       $post->delete();
       return redirect()->route('post.index')->with('danger','data yang diminta telah dihapus'); 

       
    }

}
