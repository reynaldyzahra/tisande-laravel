<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UploadFileController extends Controller
{
    //
    public function index()    {
        return view('uploadfile');
    }
    public function showUploadFile(request $request)    
    {
        $file = $request->file('epub');

        //Display FIle Name

        echo 'File Name: '.$file->getClientOriginalExtension();
        echo '<br>';

        //Disply file real path 
        echo $file->getRealPath();
        echo '<br>';

        //Display file size 
        echo 'file size :' .$file->getSize();
        echo '<br>';

        //Display File Mime Type
          echo 'File Mime Type: '.$file->getMimeType();
        
          $destinationpath = 'uploads';
          $file->move($destinationpath,$file->getClientOriginalName());
          
    }
}
