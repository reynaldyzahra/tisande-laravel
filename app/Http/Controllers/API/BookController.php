<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Book;
use App\Http\Controllers\Controller;

class BookController extends Controller
{
    public $successStatus = 200;

    public function index()
    {
        $books = Book::all();
        return response()->json(['success' => $books], $this->successStatus);
    }
}   
