@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                
                @foreach ($books as $book)

                    <div class="card">
                        <div class="card-header">{{ $book->book_name }} 
                            
                                <div class="pull-right text-right">
                                <form action="{{ route('book.destroy',$book) }}" method="POST">
                                    {{csrf_field()}}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn btn-xs btn-danger">Delete</button>
                             
                                </form>
                                </div>
                        </div>
                        
                        <div class="card-body">
                            <p>{{ $book->description }}</p>
                        </div>

                    </div>
                    <br>
                @endforeach

            </div>
        </div>
    </div>
@endsection