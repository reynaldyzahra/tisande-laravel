@extends('layouts.app')

@section('content')
    <div class="container">
        <form class="" action="{{route('book.store')}}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="">book_cover</label>
                <input type="text" class="form-control" name="book_cover" placeholder="Post Title">
            </div>
            <div class="form-group">
                <label for="">book_name</label>
                <input type="text" class="form-control" name="book_name" placeholder="Post Title">
            </div>

        
            {{-- <div class="form-group">
                <label for="">Category ID</label>
                <select name="category_id" id="" class="form-control">
                    @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div> --}}

            <div class="form-group">
                <label for="">description</label>
                <textarea name="description" rows="5" class="form-control" placeholder="Content Post"></textarea>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" value="save">
            </div>

        </form>
    </div>
@endsection