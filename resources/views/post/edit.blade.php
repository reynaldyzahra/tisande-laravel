@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-offset-2">
                <form class="" action="{{route('post.update',$post)}}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                      <div class="form-group">
                        <label for="">Title</label>
                        <input type="text" class="form-control" name="title" placeholder="Post Title" value="{{ $post->title }}">
                    </div>
                    <div class="form-group">
                        <label for="">Category ID</label>
                        <select name="category_id" id="" class="form-control">
                            @foreach ($categories as $category)
                        <option 
                        value="{{ $category->id }}"
                            @if ($category->id == $post->category_id)
                                selected
                            @endif
                        >{{$category->name}}
                        </option>
                            @endforeach
                        </select>
                    </div>
        
                    <div class="form-group">
                        <label for="">Content</label>   
                    <textarea name="content" rows="5" class="form-control" placeholder="Content Post" >{{ $post->content }}</textarea>
                    </div>
        
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="save">
                    </div>
        
                </form>
            </div>
        </div>
    </div>
@endsection