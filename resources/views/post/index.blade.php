@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                
                @foreach ($posts as $post)

                    <div class="card">
                        <div class="card-header">{{ $post->title }} 
                            
                                <div class="pull-right text-right">
                                <form action="{{ route('post.destroy',$post) }}" method="POST">
                                    {{csrf_field()}}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn btn-xs btn-danger">Delete</button>
                             
                                </form>
                                </div>
                        </div>
                        
                        <div class="card-body">
                            <p>{{ $post->content }}</p>
                        </div>

                    </div>
                    <br>
                @endforeach

            </div>
        </div>
    </div>
@endsection