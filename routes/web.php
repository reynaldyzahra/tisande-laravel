<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->middleware('checkAuth');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/post', 'Postcontroller@index')->name('post.index');
Route::get('/post/create', 'PostController@create')->name('post.create');
Route::post('/post/create','PostController@store')->name('post.store');
Route::get('/post/{post}/edit', 'PostController@edit')->name('post.edit');
Route::patch('/post/{post}/edit', 'PostController@update')->name('post.update');
Route::delete('/post/{post}/delete','PostController@destroy')->name('post.destroy');
Route::get('/book', 'BookController@index')->name('book.index');
Route::get('/book/create', 'bookController@create')->name('book.create');
Route::post('/book/create','bookController@store')->name('book.store');
Route::delete('/book/{book}/delete','bookController@destroy')->name('book.destroy');
// Route::get('/uploadfile', 'UploadFileController@index');
// Route::post('/book/create', 'BookController@store')->name('book.store');